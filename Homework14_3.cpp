#include <iostream>
#include <string>

int main()
{
    std::string testStr{"Some test string"};

    std::cout << testStr << '\n'
              << "Size: " << testStr.size() << '\n'
              << "First char: " << testStr.front() << '\n'
              << "Last char: " << testStr.back() << '\n';
}
